mod interceptor;

use interceptor::GetInterceptorError;

use std::process;

enum AppError<E> {
    InterceptorError(E),
    BindError,
}

fn exit_message(err: AppError<&str>) -> i32 {
    match err {
        AppError::BindError => {
            eprintln!("Failed to bind key interceptor.");
            3
        }
        AppError::InterceptorError(e) => {
            eprintln!("{}", e);
            2
        }
    }
}

fn run() -> Result<(), AppError<&'static str>> {
    let interceptor = match interceptor::get_platform_interceptor() {
        Ok(i) => Ok(i),
        Err(GetInterceptorError::UndefinedPlatform) => {
            let err = AppError::InterceptorError("No interceptor defined for your platform");
            Err(err)
        }
    }?;

    if let Err(_) = interceptor.bind() {
        return Err(AppError::BindError);
    }

    Ok(())
}

fn main() {
    process::exit(match run() {
        Ok(_) => 0,
        Err(err) => exit_message(err),
    });
}
