mod platform;

#[cfg(macos)]
use platform::mac::MacInterceptor;
#[cfg(windows)]
use platform::windows::WindowsInterceptor;

use std::{cmp, env, fmt};

#[derive(cmp::PartialEq, fmt::Debug)]
pub enum InterceptorError {
    BindError,
}

pub type BindResult<T> = Result<T, InterceptorError>;

pub trait Interceptor {
    fn bind(&self) -> BindResult<()>;
}

pub enum GetInterceptorError {
    UndefinedPlatform,
}

pub fn get_platform_interceptor() -> Result<Box<dyn Interceptor>, GetInterceptorError> {
    match env::consts::OS {
        #[cfg(windows)]
        "windows" => Ok(Box::new(WindowsInterceptor::keyboard())),
        #[cfg(macos)]
        "macos" => Ok(Box::new(MacInterceptor::default())),
        _ => Err(GetInterceptorError::UndefinedPlatform),
    }
}
