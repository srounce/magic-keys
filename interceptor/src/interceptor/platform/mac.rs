use crate::interceptor::{BindResult, Interceptor, InterceptorError};

#[derive(Default)]
pub struct MacInterceptor;

impl Interceptor for MacInterceptor {
    fn bind(&self) -> BindResult<()> {
        if false {
            return Err(InterceptorError::BindError);
        }

        Ok(())
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn mac_interceptor_can_bind() {
        let interceptor = MacInterceptor::default();
        let result = interceptor.bind();
        let expected = Ok(());

        assert_eq!(expected, result);
    }
}
