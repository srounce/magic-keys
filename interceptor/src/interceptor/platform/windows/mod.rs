mod message_loop;
use message_loop::MessageLoop;
mod keyboard_hook;
use keyboard_hook::{keyboard_hook, Hook, KeyboardHook};

use crate::interceptor::{BindResult, Interceptor, InterceptorError};

use std::ffi::OsStr;
use std::iter::once;
use std::os::windows::ffi::OsStrExt;
use std::ptr::null_mut;
use std::thread;

use winapi::um::winuser::{MessageBoxW, MB_OK};

pub struct WindowsInterceptor {
    message_loop: Box<MessageLoop>,
    hook: KeyboardHook,
}

impl WindowsInterceptor {
    pub fn new(hook: KeyboardHook) -> WindowsInterceptor {
        let message_loop = MessageLoop::new();
        let message_loop = Box::new(message_loop);

        WindowsInterceptor {
            message_loop,
            hook: hook,
        }
    }

    pub fn keyboard() -> WindowsInterceptor {
        let hook = keyboard_hook();
        WindowsInterceptor::new(hook)
    }

    pub fn hook_input<'a>(&'a self) -> Result<(), ()> {
        let message_loop = self.message_loop.clone();
        let hook = self.hook.clone();

        thread::spawn(move || -> Result<(), ()> {
            hook.hello();

            message_loop.start().map_err(|_| ())?;

            Ok(())
        });

        Ok(())
    }
}

impl Interceptor for WindowsInterceptor {
    fn bind<'a>(&self) -> BindResult<()> {
        if let Err(_) = self.hook_input() {
            return Err(InterceptorError::BindError);
        }

        let message_box_text: Vec<u16> = OsStr::new("Woo").encode_wide().chain(once(0)).collect();
        let _message_box_handle =
            unsafe { MessageBoxW(null_mut(), message_box_text.as_ptr(), null_mut(), MB_OK) };

        Ok(())
    }
}

#[cfg(test)]
mod test {
    // use super::*;
    // use std::time::Duration;

    // struct MockKeyboardHook {

    // }

    // impl Hook for MockKeyboardHook {

    // }

    #[test]
    fn windows_interceptor_starts_message_loop() {
        //     let hook = Box::new(MockKeyboardHook {});
        //     let interceptor = WindowsInterceptor::new(hook);
        //     let result = interceptor.bind();
        //     thread::sleep(Duration::from_millis(10));
        //     let expected = Ok(());

        //     assert_eq!(expected, result);
    }
}
