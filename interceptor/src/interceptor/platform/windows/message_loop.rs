use std::{cmp, fmt};

use std::ptr;

use winapi::um::winuser::{DispatchMessageW, GetMessageW, TranslateMessage, WaitMessage, MSG};

#[derive(cmp::PartialEq, fmt::Debug, Copy, Clone)]
pub enum MessageLoopError {}

#[derive(Clone, fmt::Debug)]
pub struct MessageLoop {}

impl MessageLoop {
    pub fn new() -> MessageLoop {
        MessageLoop {}
    }

    pub fn start(&self) -> Result<(), MessageLoopError> {
        loop {
            println!("[Message loop] go");

            let mut msg: MSG = MSG::default();
            let msg_state;
            unsafe {
                WaitMessage();
                msg_state = GetMessageW(&mut msg as *mut _, ptr::null_mut(), 0, 0);
            };

            println!("[Message loop] Message: {message:?}", message = "???");

            match msg_state {
                0 => {
                    println!("[Message loop] woop");
                    break;
                }
                1..=std::i32::MAX => unsafe {
                    TranslateMessage(&mut msg as *mut _);
                    DispatchMessageW(&mut msg as *mut _);
                },
                std::i32::MIN..=-1 => {
                    println!("[Message loop] error");
                }
            };
        }
        Ok(())
    }
}
