use std::fmt;
use std::sync::Once;
use std::{mem, ptr::null_mut, thread};

use winapi::ctypes::c_int;
use winapi::shared::minwindef::{LPARAM, LRESULT, WPARAM};
use winapi::um::winuser::{
    SetWindowsHookExW, KBDLLHOOKSTRUCT, VK_CONTROL, VK_RCONTROL, VK_RETURN, VK_RSHIFT, VK_SHIFT,
    VK_SPACE, VK_TAB, WH_KEYBOARD_LL,
};

pub struct RawKBState(KBDLLHOOKSTRUCT);

impl fmt::Debug for RawKBState {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "{{ vkCode: {}, scanCode: {}, flags: {}, time: {} }}",
            self.0.vkCode, self.0.scanCode, self.0.flags, self.0.time
        )?;
        Ok(())
    }
}

pub trait Hook {
    fn hello(&self) {
        println!("[KeyboardHook] Hello");
    }

    fn notify(&self, state: RawKBState) {
        println!("[KeyboardHook] Notify {:?}", state);
    }
}

#[derive(Clone)]
pub struct KeyboardHook {
    keys: Vec<u16>,
}

impl KeyboardHook {
    fn new() -> KeyboardHook {
        let hook = KeyboardHook { keys: vec![] };
        hook.bind_hook();
        hook
    }

    fn bind_hook(&self) {
        let _hook_handle =
            unsafe { SetWindowsHookExW(WH_KEYBOARD_LL, Some(keyboard_ll_hook), null_mut(), 0) };
    }
}

impl Hook for KeyboardHook {}

pub fn keyboard_hook() -> KeyboardHook {
    static mut INSTANCE: *const KeyboardHook = 0 as *const KeyboardHook;
    static ONCE: Once = Once::new();

    unsafe {
        ONCE.call_once(|| {
            let instance = KeyboardHook::new();

            INSTANCE = mem::transmute(Box::new(instance));
        });

        (*INSTANCE).clone()
    }
}

unsafe extern "system" fn keyboard_ll_hook(
    _code: c_int,
    w_param: WPARAM,
    l_param: LPARAM,
) -> LRESULT {
    thread::spawn(move || {
        keyboard_hook().hello();

        let kb_state: KBDLLHOOKSTRUCT = *(l_param as *const KBDLLHOOKSTRUCT);

        keyboard_hook().notify(RawKBState(kb_state));

        let key_code = kb_state.vkCode;
        let key_name = match key_code as i32 {
            VK_SPACE => String::from("<space>"),
            VK_SHIFT => String::from("<shift>"),
            VK_RSHIFT => String::from("<r-shift>"),
            VK_CONTROL => String::from("<control>"),
            VK_RCONTROL => String::from("<r-control>"),
            VK_RETURN => String::from("<return>"),
            VK_TAB => String::from("<tab>"),
            _ => {
                let key_char = key_code as u8 as char;
                key_char.to_string()
            }
        };
        println!(
            "[Hook] key {key_name} ({key_code}), wparam {w_param:b}, kb_state {l_param:#x}",
            key_code = kb_state.vkCode,
            key_name = key_name,
            w_param = w_param,
            l_param = l_param
        );
    });

    1
}
